#[derive(Debug)]
pub enum Command {
    Start,
    Stop,
    Speed(i32),
}
