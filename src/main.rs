#[macro_use] extern crate lalrpop_util;

lalrpop_mod!(pub cli);

mod grammar;

use std::io;
use std::io::prelude::*;

fn main() {
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let line = line.unwrap();
        println!("{:?}", cli::CommandParser::new().parse(line.as_ref()));
    }
}
